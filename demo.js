var myApp = angular.module('myApp', []);
myApp.controller('mycontroller', function ($scope) {
    $scope.name = "Fareeha";
    $scope.age = 21;
    $scope.products = [
        {name: "Chocolate Cake", price: 2000},
        {name: "Mango Cake", price: 3000}
    ];
    $scope.cart = [

        {name: "Chocolate Cake", price: 2000},
        {name: "Mango Cake", price: 3000},
        {name: "yolo Cake", price: 3000}

    ];
    $scope.cart = [];
    $scope.addToCart = function(item) {
        item.qty = 1;
        $scope.cart.push(item);
        $scope.total=$scope.total + item.price;
    }

});

